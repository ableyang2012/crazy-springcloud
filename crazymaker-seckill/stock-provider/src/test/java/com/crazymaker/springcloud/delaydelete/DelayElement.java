package com.crazymaker.springcloud.delaydelete;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayElement<T> implements Delayed {
    private final T t;
    private final long delayTime;

    public DelayElement(T t, long delayTime) {
        this.t=t;
        this.delayTime=delayTime;
    }

    public T getT() {

        return  t;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return delayTime;
    }

    @Override
    public int compareTo(Delayed o) {
        return 0;
    }
}
