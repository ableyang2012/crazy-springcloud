@echo off

set demo_jar=D:\dev\crazy-springcloud\crazy-springcloud\crazymaker-demo\demo-provider\target\demo-provider-1.0-SNAPSHOT.jar

set demo_jar_dest=D:\virtual\workcluster\chapter26\

XCOPY  %demo_jar%  %demo_jar_dest%   /Y


set demo_shell=D:\dev\crazy-springcloud\crazy-springcloud\crazymaker-demo\demo-provider\src\main\assembly\bin\deploy-for-jvm-demo.sh

set demo_shell_dest=D:\virtual\workcluster\chapter26\

XCOPY  %demo_shell%  %demo_shell_dest%   /Y