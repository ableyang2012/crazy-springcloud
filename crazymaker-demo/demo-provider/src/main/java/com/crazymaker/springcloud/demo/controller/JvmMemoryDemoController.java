package com.crazymaker.springcloud.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.crazymaker.springcloud.common.result.RestOut;
import com.crazymaker.springcloud.common.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@Api(value = "JvmMemoryDemo", tags = {"JvmMemory"})

@RestController
@RequestMapping("/jvm/file")
public class JvmMemoryDemoController {

    private int count=0;

    static class TestMemory {
        int foo;
    }

    //声明缓存对象
    private static final Map map = new HashMap();


    @GetMapping("/addObject/v1")
    @ApiOperation(value = "添加对象到缓存")
    public RestOut<JSONObject> addObject()
    {

        //循环添加对象到缓存
        for(int i=0; i<1_000_000;i++){
            TestMemory t = new TestMemory();
            map.put("key"+i,t);
        }
        JSONObject data = new JSONObject();

        data.put("第N次操作：", ++count);
        return RestOut.success(data).setRespMsg("操作成功");

    }

}


