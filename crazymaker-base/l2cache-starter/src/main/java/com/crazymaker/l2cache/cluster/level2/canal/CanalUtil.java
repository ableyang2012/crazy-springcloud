package com.crazymaker.l2cache.cluster.level2.canal;

import com.alibaba.otter.canal.protocol.FlatMessage;
import com.crazymaker.springcloud.common.util.JsonUtil;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
public class CanalUtil {


    /**
     * 转换Canal的FlatMessage中data成泛型对象
     *
     * @param flatMessage Canal发送MQ信息
     * @return 泛型对象集合
     */
    public static <T> Set<T> getCanalData(FlatMessage flatMessage, Class<T> type) {
        List<Map<String, String>> sourceData = flatMessage.getData();
        Set<T> targetData = Sets.newHashSetWithExpectedSize(sourceData.size());
        for (Map<String, String> map : sourceData) {
            T t = JsonUtil.mapToPojo(map, type);
            targetData.add(t);
        }
        return targetData;
    }

    public static List<Map<String, String>> getCanalDataList(FlatMessage flatMessage) {
        return flatMessage.getData();

    }
}
